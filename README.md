##Waveform
This Windows application does as the title suggests: it renders a waveform from audio
samples loaded in a separate thread before drawing begins.

More information to this document will be added in later commits.

##Building
I have already included the neccessary headers and libraries, otherwise you will need
the Windows SDK if you are using an express edition of Visual Studio.

##Acknowledgements
Waveform uses the `libsndfile` library to extract samples from audio files. More information
can be found at http://www.mega-nerd.com/libsndfile/

This application uses a modified version GLFW to manage OpenGL contexts easily. You can
retrieve the latest version of GLFW from http://www.glfw.org

