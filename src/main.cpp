#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <GLFW/glfw3native.h>
#include <sndfile.h>

#include "waveform.h"
#include "resource.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

using std::vector;
using std::string;

HINSTANCE instance;

static void OpenFileDialog(HWND hWnd)
{
	char strFilename[260];

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWnd;
	//Set the filename to strFilename
	ofn.lpstrFile = strFilename;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(strFilename);
	ofn.lpstrFilter = "All Files\0*.*\0WAV\0*.wav\0AIFF\0*.aiff\0AIFC\0*.aifc\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	//Display the dialog box
	if (GetOpenFileNameA(&ofn) == TRUE)
	{
		DWORD hThreadID;
		WaveformContext.m_filename = ofn.lpstrFile;

		HANDLE hThread = CreateThread(NULL, 0, WaveOpen, NULL, NULL, &hThreadID);
		CloseHandle(hThread);
	}
}

static void glerror_callback(int error, const char *description)
{
	fprintf(stderr, "[glfw]: Error %d, \"%s\"", error, description);
}

static void gldrop_callback(GLFWwindow *window, int nfiles, const char **strFiles)
{
	if (nfiles == 1)
	{
		DWORD hThreadID;
		WaveformContext.m_bOpen = false;
		WaveformContext.m_filename = strFiles[0];

		HANDLE hThread = CreateThread(NULL, 0, WaveOpen, NULL, NULL, &hThreadID);
		CloseHandle(hThread);
	}
}

INT_PTR WINAPI ColorDialogEvents(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			return true;
		case WM_COMMAND:
			switch (wParam) {
			case IDCANCEL:
			case IDOK:
				EndDialog(hWnd, 1);
				return 1;
			}
			break;
	}

	return 0;
}

static void glmenu_callback(GLFWwindow *window, int id)
{
	if (id == ID_FILE_OPENAUDIO)
	{
		OpenFileDialog(glfwGetWin32Window(window));
	}
	if (id == ID_HELP_ABOUT)
	{
		MessageBoxA(NULL, "Waveform, written by Ben Cottrell", "About", MB_ICONINFORMATION);
	}
	if (id == ID_EDIT_COLORS)
	{
		DialogBox(NULL, MAKEINTRESOURCE(IDD_COLOR_DIALOG), glfwGetWin32Window(window), (DLGPROC)ColorDialogEvents);
	}
	if (id == ID_FILE_EXIT)
	{
		PostQuitMessage(0);
	}
}

static void glkey_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (mods = GLFW_KEY_LEFT_CONTROL && key == GLFW_KEY_O && action == GLFW_PRESS) {
		OpenFileDialog(glfwGetWin32Window(window));
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
	instance = hInstance;
	
	GLFWwindow *window;

	glfwSetErrorCallback(glerror_callback);
	
	if (!glfwInit())
		exit(EXIT_FAILURE);

	window = glfwCreateWindow(400, 400, "Waveform - Written by Ben Cottrell", NULL, NULL);

	glfwSetKeyCallback(window, glkey_callback);
	glfwSetMenuCallback(window, glmenu_callback);
	glfwSetDropCallback(window, gldrop_callback);

	SetMenu(glfwGetWin32Window(window), LoadMenu(NULL, MAKEINTRESOURCE(IDR_APPMENU)));

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float) height;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, 1.f, -1.0f);

		glColor3f(0.7f, 0.7f, 0.7f);
		DrawLine(0, height / 2, width, height / 2, 2.f);

		if (!WaveformContext.m_filename.empty() && WaveformContext.m_nChannels != 0)
			WaveformContext.Draw(0, width, 0, height);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}