//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Waveform.rc
//
#define IDR_APPMENU                     101
#define IDD_COLOR_DIALOG                102
#define IDC_FGCOLOR                     1004
#define IDC_CUSTOM2                     1005
#define IDC_AXISCOLOR                   1005
#define ID_FILE_OPENAUDIO               40001
#define ID_FILE_SAVE40002               40002
#define ID_FILE_EXIT                    40003
#define ID_EDIT_COLORS                  40004
#define ID_TOOLS_OPTIONS                40005
#define ID_HELP_MANUAL                  40006
#define ID_HELP_ABOUT                   40007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40008
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
