#ifndef waveform_h
#define waveform_h

#include <iostream>
#include <vector>
#include <sndfile.h>
#include <gl\GL.h>
#include "resource.h"
#include <GLFW\glfw3.h>

using std::vector;
using std::string;

#define BUFFER_SIZE 512

const float LINE_SIZE = 2.f;

static void DrawLine(float x1, float y1, float x2, float y2, float size)
{
	glBegin(GL_QUADS);

	glVertex2f(x1, y1 + size);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glVertex2f(x2, y2 + size);

	glEnd();
}

static void glkey_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
static void glmenu_callback(GLFWwindow *window, int id);
static void gldrop_callback(GLFWwindow *window, int nfiles, const char **strFiles);
static void glerror_callback(int error, const char *description);

class WaveformData
{
public:
	//TODO Implement gridlines
	int Run(HINSTANCE hInstance)
	{
		instance = hInstance;
	
		GLFWwindow *window;

		glfwSetErrorCallback(glerror_callback);
	
		if (!glfwInit())
			exit(EXIT_FAILURE);

		window = glfwCreateWindow(400, 400, "Waveform - Written by Ben Cottrell", NULL, NULL);

		glfwSetKeyCallback(window, glkey_callback);
		glfwSetMenuCallback(window, glmenu_callback);
		glfwSetDropCallback(window, gldrop_callback);

		SetMenu(glfwGetWin32Window(window), LoadMenu(NULL, MAKEINTRESOURCE(IDR_APPMENU)));

		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);

		while (!glfwWindowShouldClose(window))
		{
			float ratio;
			int width, height;

			glfwGetFramebufferSize(window, &width, &height);
			ratio = width / (float) height;
			glViewport(0, 0, width, height);
			glClear(GL_COLOR_BUFFER_BIT);
		
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, width, height, 0, 1.f, -1.0f);

			glColor3f(0.7f, 0.7f, 0.7f);
			DrawLine(0, height / 2, width, height / 2, 2.f);

			if (!WaveformContext.m_filename.empty() && WaveformContext.m_nChannels != 0)
				WaveformContext.Draw(0, width, 0, height);

			glfwSwapBuffers(window);
			glfwPollEvents();
		}

		glfwDestroyWindow(window);

		glfwTerminate();
		exit(EXIT_SUCCESS);
	}

	bool Draw(int left, int right, int top, int bottom)
	{
		if (!m_bOpen)
			return false;

		float ratio = (bottom - top) / 2.f;
		ratio = ratio / 2.f;

		//Divide the segment size w.r.t. to one second and the current sample rate 
		float segmentSize = (right - left) / m_sfinfo.samplerate;

		int index = 0;
		float height = bottom;
		float width = (right - left);

		glBegin(GL_LINE_STRIP);
		glColor3f(0,0.54f,0.98f);
		ratio = (height / 2);
		for (float x=0; x < m_Samples.size(); x++) 
		{
			glColor3f(0,0.54f,0.98f);
			glVertex2f((x / (float)m_Samples.size()) * width,  ratio + m_Samples[x] * ratio);
			glColor3f(0.f, 0.4f, 0.9f);
			glVertex2f((x / (float)m_Samples.size()) * width,  ratio + (m_Samples[x] / 2) * ratio);
		}
		glEnd();
		return true;
	}
	void Quit()
	{
		
	}

	HINSTANCE instance;
	vector<float> m_Samples;
	SNDFILE *m_sndFile;
	SF_INFO m_sfinfo;
	int m_nChannels;
	string m_filename;
	bool m_bOpen;
} WaveformContext;

static DWORD WINAPI WaveOpen(LPVOID lpParam)
{
	WaveformContext.m_bOpen = false;

	//Find the number of channels in the current sound file
	if ( (WaveformContext.m_sndFile = sf_open(WaveformContext.m_filename.c_str(), SFM_READ, &WaveformContext.m_sfinfo)) == NULL )
	{
		return 1;
	}

	WaveformContext.m_Samples.clear();
	WaveformContext.m_nChannels = WaveformContext.m_sfinfo.channels;

	//Read the channel data into the list
	float *buf = (float*)malloc((WaveformContext.m_sfinfo.channels * BUFFER_SIZE) * sizeof(float));
	vector<float> samples;
	int k, m, readcount;
		
	while ( (readcount = sf_readf_float(WaveformContext.m_sndFile, buf, BUFFER_SIZE)) > 0 )
	{
		for (k=0; k < readcount; k++)
		{
			WaveformContext.m_Samples.push_back(buf[k]);
		}
	}

	WaveformContext.m_bOpen = true;

	return 0;
}



#endif
